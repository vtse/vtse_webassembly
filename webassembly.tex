% !TEX encoding = UTF-8 Unicode
\RequirePackage{scrlfile}
\ReplacePackage{scrpage2}{scrlayer-scrpage}

\documentclass[Seminar, BIF, german]{twbook}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{xcolor}
\usepackage[style=ieee]{biblatex}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{hyphsubst}

\renewcommand*{\chapterheadstartvskip}{\vspace*{1cm}}
\renewcommand*{\chapterheadendvskip}{\vspace{0.5cm}}

\addbibresource{wasm.bib}
\graphicspath{ {./figures/} }

\title{Native Performance im Web: Evaluierung des WebAssembly Projekts}
\author{Benjamin Ableidinger}
\studentnumber{1810257027}
\supervisor{DI Wolfgang Berger}
\place{Wien}
\kurzfassung{
	Die Nutzung des Webs wandelt sich ständig. 
	Während das ursprüngliche Ziel das Teilen von Dokumenten in akademischen Kreisen darstellte, dominiert das Internet heutzutage unsere Leben in Bereichen wie Bildung, Unterhaltung, Produktivität und vielen weiteren.
	Trotz Versuchen andere Programmiersprachen im Web zu etablieren hat sich Javascript als Sprache des Webs durchgesetzt. Es wird nun von einfacher UI Manipulierung bishin zu komplexen Applikationen wie Computerspielen eingesetzt.
	\\
	Aufgrund der dynamischen und zur Laufzeit verarbeiteten Natur von Javascript kann es trotz jahrelanger Optimierung für intensive Applikationen (Animation, Computerspiele,...) nicht zu nativem Code vergleichbare Performance erbringen.
	Eine Zusammenkunft aus Entwicklern verschiedener Unternehmen (Goolge, Mozilla, Microsoft, Apple,...) hat deshalb die Low-Level Sprache WebAssembly entworfen, welche kompakt im Binärformat ausgeliefert wird.
	\\\\
	Andere Projekte wie asm.js oder Native Client haben schon versucht die Probleme von Javascript, beispielsweise durch starke Einschränkung der APIs, zu minimieren.
	WebAssembly ist durch seine Unabhängigkeit von Javascript jedoch noch performanter als asm.js und verfolgt dabei noch Designziele wie Portabilität, Kompaktheit und Interoperabilität.
	\\
	Benchmarks zeigen, dass sich WebAssembly Jahr für Jahr näher an die Performance von nativem C Code annähert.
	WebAssembly kann aus verschiedensten Sprachen (C/C++, C\#, Java,...) kompiliert werden und findet Unterstützung von großen Unternehmen.
	Während von der WebAssembly Working Group nicht das Ziel verfolgt wird, Javascript zu ersetzen, kommen ers\-te Lösungen auf den Markt, die ganze Webapplikationen in WebAssembly umsetzen lassen.
	\\
	Es kann davon ausgegangen werden, dass sich WebAssembly im Umfeld der Webentwicklung etablieren wird.
}
\outline{
	The use cases of the internet are constantly changing. While the original purpose was sharing documents in academic circles, the internet now dominates areas of life like education, entertainment, productivity and many more.
	Despite attempts trying to establish other programming languages in the web, Javascript came out ahead. Its usage now ranges from simple UI manipulation to complex applications like computer games.
	Due to the dynamic and just-in-time executed nature of Javascript, it cannot deliver performance comparable to native code for resource intensive applications (animation, computer games,...).
	A group of developers of various companies (Google, Mozilla, Microsoft, Apple,...) has developed the low-level language Web\-Assembly, which is delivered in a binary format to combat the deficienties of Javascript.
	\\\\
	Other projects like asm.js or Native Client have tried to solve Javascripts problems, for example by limiting its APIs. Because of it's independency from Javascript, WebAssembly performs even better than asm.js and defines design goals like portability, compactness and interoperability.
	Benchmarks show that WebAssembly is getting closer to native performance each year. WebAssembly can be compiled from many languages including C/C++, C\#, Java,... and is supported by large companies.
	While the WebAssembly working group does not aim to replace Javascript, frameworks that allow implementing complete web applications in WebAssembly are already coming to market.
	It can be assumed that WebAssembly will be established as a standard in the field of web development.
}

\begin{document}
\maketitle

\chapter{Einleitung}
Die Implementierung von WebAssembly indiziert einen neuen Abschnitt im Umfeld der Webent\-wick\-lung.
Diese Arbeit verfolgt das Ziel, den Lesenden zu zeigen, warum WebAssembly für die Zukunft des Webs wichtig und notwendig ist.
Neben einer Übersicht der Technologie wird die Einordnung im Vergleich zu aktuellen Tools und Sprachen diskutiert und die Performanceunterschiede sowie mögliche Anwendungsgebiete aufgezeigt.
\\
Zur Findung der entsprechenden Literatur wurde auf Google Scholar nach Schlagwörtern gesucht.
Da die Designphase von WebAssembly erst 2017 \cite{RoadmapWebAssembly} abgeschlossen wurde, sind alle gefundenen Quellen seit 2017 (oder neuer) verfasst worden.
Deswegen stellt die Aktualität kein Problem dar, jedoch ist die Zahl der Arbeiten noch beschränkt.


\chapter{Einführung WebAssembly}
Um WebAssembly mit anderen Technologien vergleichen zu können und die Motivation hinter dem Projekt zu verstehen müssen zuerst eine Grundlagen bearbeitet werden.
\section{Grundlegendes zum Projekt}
\label{sec:basics}
WebAssembly ist ein portabler Low-Level Bytecode, welcher im Binärformat ausgeliefert wird.
Grundsätzlich ist die Verwendung der Technologie nicht an das Web gebunden.
WebAssembly ist lediglich eine Abstraktion über moderne Hardware und somit sprach-, hardware- und platt\-formunabhängig. \cite{haasBringingWebSpeed2017} 
\\
Immer mehr Desktopapplikationen werden ins Web übersiedelt, um Vorteile wie Plattformunabhängigkeit zu gewinnen.
Hierzu reicht aber in einigen Fällen die Performance von Javascript verglichen zu nativem C Code nicht aus,
weswegen eines der Ziele des WebAssembly Projekts die Schaffung einer Javascript Alternative mit nahezu nativer Performance ist (siehe \ref{sec:motivation}). \cite{FAQWebAssembly, haasBringingWebSpeed2017}
\\\\
Entwickelt wird der Standard von Repräsentanten der vier großen Browserhersteller: Google, Mozilla, Apple und Microsoft sowie weiteren Unternehmen.
Durch diese Zusammen\-kunft konnten die spezifizierten Features implementiert und sofort parallel in verschiedenen Browserengines auf Sinnhaftigkeit, Validität und Vollständigkeit der Definintion getestet werden. \cite{haasBringingWebSpeed2017}
\\\\
Die Entwicklung von WebAssembly wird nach dem World Wide Web Consortium (W3C) Prozess durchgeführt.
Dieser teilt die Standardisierung in sechs Phasen vom Vorschlag aus der Community bishin zum von der Working Group abgenommenen implementierten Feature ein. \cite{WebAssemblyW3CProcess}
\\\\
Erstmals wurde WebAssembly 2015 vorgestellt \cite{lukewagnerWebAssemblyLukeWagner2015}.
Neben der Gründung der WebAssembly Working Group wurde 2017 angekündigt, dass alle der vier größten Browserhersteller Web\-Assembly in einer Erstversion unterstützen \cite{bradleynelsonLaunchingWebAssemblyWorking2017}.
Schließlich wurde 2019 die WebAssembly Core Specification als offizieller Webstandard laut W3C herausgegeben (Version 1.0) \cite{WorldWideWeb2019}.
\\
Unter den zurzeit in Entwicklung stehenden Features befinden sich Exception Handling sowie Threads und Atomics \cite{RoadmapWebAssembly}.

\section{Motivation}
\label{sec:motivation}
Seit der Ausbreitung des Internets hat sich dessen Nutzen drastisch geändert. 
Während ursprünglich nur der Austausch von Dokumenten angedacht war, werden nun komplexe Websites bishin zu noch ressourcenintensiveren Anwendungen wie Computerspielen in Browsern ausgeführt.
Nach einigen Ansätzen wie ActiveX, Java und Flash hat sich Javascript als einzige Programmiersprache im Web etabliert.
Mit der zunehmenden Komplexität der Webanwendungen stößt jedoch auch Javascript an seine Grenzen, da es aufgrund seines Laufzeitoverheads durch Mechanismen wie dynamische Typisierung, Garbage Collection, Interpretation zur Laufzeit und die notwendige Verifizierung des Codes keine nahezu native Performance liefern kann. \cite{haasBringingWebSpeed2017}
\\\\
Deswegen hat sich das WebAssembly Projekt das Ziel gesetzt, eine als performanter, sicherer, portabler Low-Level Code entworfene Sprache zu entwickeln \cite{haasBringingWebSpeed2017}.
Die Technologie kann als Nachfolger von Javascript Optimierungen wie asm.js und NativeScript gesehen werden und wurde als erste relevante Sprache vollkommen mit formeller Semantik beschrieben \cite{haasBringingWebSpeed2017}.
\\
Ein wesentlicher Vorteil des neuen Standards gegenüber asm.js ist das kompakte Binärformat. 
Durch dieses kann WebAssembly Code wesentlich schneller decodiert werden, als Javascript geparsed werden kann \cite{FAQWebAssembly} (dieses wird im besten Fall als minifizierter Text übertragen \cite{haasBringingWebSpeed2017}).
Außerdem kann WebAssembly neue Features wesentlich effizienter einführen, da nicht zuerst der Javascript Standard adaptiert werden muss um dann das Feature in beispielsweise asm.js zu implementieren. \cite{haasBringingWebSpeed2017}
\\
Der WebAssembly Vorläufer, Native Client (NaCl), ist ein Subset des x86 Maschinencodes und somit nicht plattformunabhängig. 
Die Weiterführung Portable NaCl (PNaCl) behebt zwar dieses Problem, wird aber neben anderen Problemen nur von Chrome unterstützt und ist somit grundsätzlich nicht portabel.
\cite{haasBringingWebSpeed2017}
\\\\
Die WebAssembly Core Spezifikation definiert unter anderem Designziele wie nativ-ähnliche Ausführungsgeschwindigkeit, Sicherheit durch Ausführung in einer Sandbox, Hardware-/Sprach- und Plattformunabhängigkeit, Kompaktheit, Modularität und Effizienz.
\cite{WebAssemblyCoreSpecification}

\section{Browsersupport}
WebAssembly wird von den gängigen Browserengines (Chrome, Firefox, Safari) unterstützt \cite{RoadmapWebAssembly}. 
Erstmals wurde Support von allen großen Browsern durch Mozilla 2017 erklärt \cite{mcconnellWebAssemblySupportNow2017}, wobei WebAssembly von Chrome seit Version 57, von Firefox seit Version 52, von Edge seit Version 16 und von Safari seit Version 11 unterstützt wird \cite{WebAssembly}.
WebAssembly kann jedoch auch in asm.js Code konvertiert werden (Polyfilling), um in älteren Browsern ausgeführt zu werden \cite{FAQWebAssembly}.

\section{Relevante Technologien}
Um die Performance vergleichen zu können (Kapitel \ref{sec:benchmarking}) werden im nachfolgenden die Technologien asm.js und Emscripten umrissen.
\subsection{asm.js}
Bei asm.js handelt es sich um ein Subset von Javascript, das dessen dynamisches Verhalten einschränkt.
Zum Beispiel müssen Typen festgelegt werden.
Durch solche Optimierungen kann performanterer Javascript Code erzeugt werden.
Erstellt wird Low-Level asm.js Code automatisch beispielsweise mit Emscripten aus C oder C++ (siehe \ref{sec:emscripten}).
Jedoch verbleiben trotzdem Javascript bedingte Limitierungen (unter anderem hat Javascript keine 64-bit Integer).
\cite{zakaiFastPhysicsWeb2018}

\subsection{Emscripten}
\label{sec:emscripten}
Das Emscripten Projekt bietet die Möglichkeit, C oder C++ Code in asm.js, WebAssembly und weitere zu übersetzen.
Somit ergibt sich die Möglichkeit C/C++ Code für die Verwendung im Web zu kompilieren.
\cite{zakaiFastPhysicsWeb2018}

\chapter{Performance Benchmarking}
\label{sec:benchmarking}
Da mit Emscripten C Code direkt in WebAssembly Code umgewandelt werden kann, liegt ein direkter Performancevergleich nahe.
Die WebAssembly Working Group hat dazu das Benchmarking-Tool PolyBenchC \cite{PolyBenchCPolyhedralBenchmark} in einer WebAssembly-Umgebung ausgeführt,
wobei die resultierenden WebAssembly Module in Javascript eingebettet und ausgeführt wurden (Resultate siehe Abbildung \ref{fig:emscripten_polybench}).
Sieben der PolyBenchC Benchmarks sind innherhalb eines 10\% Unterschieds zu nativem C, nahezu alle unterscheiden sich weniger als 100\%.
\cite{haasBringingWebSpeed2017}
\\
\begin{figure}[b]
	\centering
	\begin{subfigure}{.55\textwidth}
		\centering
		\includegraphics[width=\linewidth]{benchmark_wasm_vs_c_wg}
		\caption{2017 \cite{haasBringingWebSpeed2017}}
		\label{fig:emscripten_polybench}
	\end{subfigure}%
	\begin{subfigure}{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{benchmark_wasm_vs_c_comp}
		\caption{Seit 2017 \cite{jangdaNotFastAnalyzing2019}}
		\label{fig:benchmark_wasm_vs_c_comp}
	\end{subfigure}
	\caption{PolyBenchC: WebAssembly verglichen mit nativem Code}
\end{figure}
\noindent
Anzumerken ist der Unterschied der beiden getesteten Javascript Engines V8 (Chrome) und SpiderMonkey (Firefox).
Gesamt kann hier keine schnellere Engine festgestellt werden. \cite{haasBringingWebSpeed2017}
\\\\
Weiters kann ein Performancevergleich mit asm.js durchgeführt werden, indem mittels Emscripten auf asm.js Code kompiliert wird.
Durchschnittlich ist WebAssembly hier 33,7\% schneller als asm.js, hervorzuheben ist besonders die Performance bei der Validierung.
Hier braucht WebAssembly unter 3\% der Zeit die es braucht, um asm.js Code zu validieren. \cite{haasBringingWebSpeed2017} 
\\\\
In der Arbeit \citetitle{jangdaNotFastAnalyzing2019} (2019) werden PolyBenchC Benchmarks zwischen 2017 und 2019 verglichen. 
Die Resultate sind in Abbildung \ref{fig:benchmark_wasm_vs_c_comp} zu sehen.
Zu beachten sind die steigende Zahl an Benchmarks, die unter 10\% Performanceabfall verglichen zu nativem C Code zeigen.
Im Jahr 2017 fallen sieben Tests in diese Kategorie \cite{haasBringingWebSpeed2017}, 2019 sind es 13.
\cite{jangdaNotFastAnalyzing2019}
\noindent
Es ist jedoch die Aussagekraft von PolyBenchC Benchmarks für WebAssembly Performancevergleiche anzuzweifeln, da einer der offiziellen Use-Cases von WebAssembly \cite{UseCasesWebAssembly} genau von PolyBenchC abgedeckt wird.
Deswegen müssen weitere Benchmarks durchgeführt werden, um die Performance von WebAssembly adäquat analysieren zu können.
Die SPEC CPU Benchmark Suite bietet sich hierzu an. 
Um diese im WebAssembly Umfeld ausführen zu können, muss jedoch eine Alternative zu Emscripten gefunden werden, die eine Vielzahl an Unix Aufrufen unterstützt (Synchrone I/O Operationen,...).
Dazu hat man in der Arbeit \citetitle{jangdaNotFastAnalyzing2019} Browsix adaptiert, um mit WebAssembly arbeiten zu können \cite{jangdaNotFastAnalyzing2019}.
\begin{figure}[t]
	\centering
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{benchmark_browsix-wasm_spec}
		\caption{SPEC CPU gegen nativ}
		\label{fig:benchmark_browsix-wasm_spec}
	\end{subfigure}%
	\begin{subfigure}{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{benchmark_browsix-wasm_spec_vs_asm}
		\caption{SPEC CPU gegen asm.js}
		\label{fig:benchmark_browsix-wasm_spec_vs_asm}
	\end{subfigure}
	\caption{Benchmarks mit Browsix-Wasm}
\end{figure}
\noindent
Die Verwendung von Browsix-Wasm statt Emscripten bringt nur minimalen Overhead mit sich (Maximal 1,2\% schlechter verglichen mit Abbildung \ref{fig:emscripten_polybench}).
In Abbildung \ref{fig:benchmark_browsix-wasm_spec} sind die Resultate des SPEC CPU Benchmarks ersichtlich. Diese Benchmarks verwenden umfassend Systemaufrufe, die nur mit Browsix-Wasm möglich sind.
Durchschnittlich ist WebAssembly in Chrome 1,55 mal langsamer als nativer C Code, während WebAssembly in Firefox 1,45 mal langsamer ist.
\cite{jangdaNotFastAnalyzing2019}
\\\\
In Abbildung \ref{fig:benchmark_browsix-wasm_spec_vs_asm} werden die SPEC CPU Ergebnisse von WebAssembly mit denen von asm.js verglichen.
Im Durchschnitt ist WebAssembly 1,54 mal (Chrome) und 1,39 mal (Firefox) schneller als asm.js.
\cite{jangdaNotFastAnalyzing2019}

\chapter{Diskussion und Ausblick}
Auf Basis der in den vorhergehenden Kapiteln erarbeiteten Informationen kann nun der aktuelle Stand sowie die Zukunft des WebAssembly Projekts evaluiert werden. 
\section{Stand des Projekts}
Das WebAssembly Projekt verspricht durch Verwendung spezifizierter Prozesse (siehe \ref{sec:basics}), zukünftig effizient neue Features implementieren zu können.
Der verwendete W3C Ablauf zur Standardisierung ermöglicht das Mitwirken der WebAssembly Community, sichert jedoch die Qualität durch die gebildete Working Group.
\\
Weil diese Working Group Mitglieder aller großen Browserhersteller hat kann davon ausgegangen werden, dass aus WebAssembly kein Nischenprodukt wird, sondern eine Abdeckung der meisten Browsernutzenden besteht (92,97\% der globalen Benutzer und Benutzerinnen laut caniuse.com am 05.12.2020 \cite{CanUseSupport}).


\section{Einordnung ins Umfeld der modernen und zukünftigen Webentwicklung}
Die Unterstützung großer Unternehmen wie Google, Mozilla, Microsoft und Apple deutet auf eine flächendeckende Verbreitung von WebAssembly hin.
Nun ergibt sich die Frage, wo Web\-Assembly zur Anwendung kommen wird und besonders wie die Technologie nicht eingesetzt werden sollte.
\\\\
Im FAQ Bereich der WebAssembly Website wird betont, dass das Ziel ausdrücklich nicht ist, Javascript zu ersetzen.
Man wolle die Möglichkeit schaffen, performancekritische Teile von Javascriptapplikationen in WebAssembly auslagern zu können.
Dabei ist nicht nur eigens erschaffener Code gemeint. 
Es können ganze Module von Drittanbietern eingebaut werden, ohne als Webentwickelnder eine Sprache außerhalb von Javascript beherrschen zu müssen.
Trotzdem erhält man die Performancevorteile von WebAssembly.
Beispielsweise sollen UI Interaktionen in Javascript gehalten werden. 
Weiters kann Javascript genutzt werden, um die Grundstruktur einer Webapplikation zu bauen und WebAssembly Module zu verbinden.
\cite{FAQWebAssembly}

\subsection{Use Cases}
Die WebAssembly Website gibt unter anderem Use Cases wie besseres Kompilieren aus anderen Sprachen, Bild-/Videobearbeitung, Computerspiele, Bilderkennung und wissenschaftliche Visualisierung und Simulation an.
\cite{UseCasesWebAssembly}
\\\\
Zusammenfassend kann angenommen werden, dass sich WebAssembly für leistungshungrige Anwendungen eignet und dort auch von Javascriptentwickelnden ohne C/C++ Kenntnissen integrieren lässt.
\\
Beim Entwurf von Webseiten beziehungsweise Webapplikationen ist davon auszugehen, dass aufgrund des vorhandenen Ökosystems der Frameworks und der Community mit Javascript schneller und unkomplizierter gearbeitet werden kann.
Wenn Performance keine große Rolle spielt haben die dynamischen Komponenten von Javascript Vorteile wie kurze Designphasen, das Ersparen des Kompilierschrittes und somit das direkte Schreiben des ausgeführeten Codes (dieser kann auch im Browser direkt eingesehen werden \cite{haasBringingWebSpeed2017}). 

\subsection{Adaption durch Branchenführer}
Einer der ersten Anwender der Technologie ist Microsoft mit dem Singe Page Application (SPA) Framework Blazor.
Ähnlich wie andere SPA Frameworks zielt Blazor darauf ab, UI Updates unkompliziert nach dem Laden von Daten durchzuführen, ohne die Seite neu laden zu müssen.
Der wesentliche Unterschied ist die Verwendung von C\# als Programmiersprache, die dann zu WebAssembly Code kompiliert wird.
Microsoft argumentiert dadurch Vorteile wie die Verwendung von C\# am ganzen Technologiestack (Frontend und Backend) und dadurch das Teilen von Bibliotheken zwischen Front- und Backend.
\cite{danielrothIntroductionASPNET2020}
\\\\
Hier ist jedoch ein Abweichen von der initialen Zielsetzung von WebAssembly zu erkennen. 
Mit Blazor können und sollen ganze Webapplikation gebaut werden. 
Die Blazor Dokumentation er\-klärt die Zusammenarbeit mit Javascript, allerdings im Bezug auf Abwärtskompatibilität und die Verwendung von Bibliotheken von Drittanbietern.
Jedenfalls werden umfassende Möglichkeiten geboten, mit dem User Interface zu interagieren.
\\\\
Ein anderer der WebAssembly Use Cases ist das Ausführen komplexer Computerspiele in Browsern. 
Als einer der Branchenführer im Bereich der Computerspiele-Engines hat Unity als Kompilierziel schon 2018 von asm.js auf WebAssembly gewechselt \cite{marcotrivellatoWebAssemblyHereUnity2018}.
Die Unreal Engine hat diesen Schritt bereits 2017 getan \cite{jeffwilsonUnrealEngine182017}. Beide sehen dadurch Verbesserungen in der Ladezeit (durch das Binärformat) und generellen Performance.
Durch das Einführen von Threading Support ist mit weiteren Performanceverbesserungen zu rechnen.
Zum Zeitpunkt dieser Arbeit wird WebAssembly Threading bereits von Chrome und Firefox unterstützt, wobei das Feature noch nicht vollkommen spezifiziert ist.
\cite{RoadmapWebAssembly}

\subsection{Zukunft von WebAssembly}
Die Liste von Sprachen mit der Möglichkeit der Ausführung des Codes in einer WebAssembly Umgebung wächst stetig.
\cite{appcypherAppcypherAwesomewasmlangs2020}
\\
Mit Blazor hat Microsoft WebAssembly schon tief in sein Ökosystem integriert, andere Sprachen haben ähnliche Ansätze (Zum Beispiel Java TeaVM mit Flavour \cite{TeaVM}).
Es ist anzunehmen, dass Microsoft WebAssembly bereits umfassend anwendet, da der Konzern in der WebAssembly Working Group vertreten ist.
\chapter{Konklusion}
Während WebAssembly selbst nicht auf das Ersetzen von Javascript abzielt, kann man am Beispiel von Microsoft schon erkennen, dass die Anwendung der Technologie auf verschiedenste Weisen möglich ist.
Wie sehr sich die Use Cases in Zukunft wandeln werden bleibt abzuwarten.
Die Kombination aus Vielseitigkeit, Verlässlichkeit durch definierte Entwicklungsprozesse und bessere Performance (Kapitel \ref{sec:benchmarking}) verspricht die kontinuierliche Anwendung sowie den weit verbreiteten Einsatz von WebAssembly in der Zukunft des Webs.
\clearpage
\printbibliography

\clearpage
\listoffigures

\phantomsection
\addcontentsline{toc}{chapter}{Abkürzungsverzeichnis}
\chapter*{Abkürzungsverzeichnis}
\begin{acronym}[XXXXX]
	\acro{JS}[JS]{Javascript}
	\acro{W3C}[W3C]{World Wide Web Consortium}
	\acro{NaCl}[NaCl]{Native Client}
	\acro{PNaCl}[PNaCl]{Portable Native Client}
	\acro{FAQ}[FAQ]{Frequently Asked Questions}
	\acro{CAD}[CAD]{Computer-aided Design}
	\acro{SPA}[SPA]{Single Page Application}
	\acro{UI}[UI]{User Interface}
	\acro{API}[API]{Application Programming Interface}
	\acro{Wasm}[Wasm]{WebAssembly}
\end{acronym}

\end{document}